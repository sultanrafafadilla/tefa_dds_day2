var restify = require('restify');
 
const server = restify.createServer({
  name: 'myapp',
  version: '1.0.0'
});
 
server.use(restify.plugins.acceptParser(server.acceptable));
server.use(restify.plugins.queryParser());
server.use(restify.plugins.bodyParser());
 
//menampikans
server.post('/www', function (req, res, next) {
  res.send(req.body);
  return next();
});

server.get('/TEFA/:alas/:tinggi', function(req,res,next){
    let luas = (req.params.alas/ 2) * req.params.tinggi ;
    res.send("luas = "+luas);
    return next();
});

server.post('/TEFA', function(req,res,next){
    let luas = (req.body.alas * req.body.tinggi)/2;
    res.send("luas = "+luas);
});

 
server.listen(8080, function () {
  console.log('%s listening at %s', server.name, server.url);
});