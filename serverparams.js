var restify = require('restify');
 
const server = restify.createServer({
  name: 'myapp',
  version: '1.0.0'
});
 
server.use(restify.plugins.acceptParser(server.acceptable));
server.use(restify.plugins.queryParser());
server.use(restify.plugins.bodyParser());
 
server.get('/www/:name', function (req, res, next) {
  res.send(req.params);
  return next();
});
// * menggunakan params

server.get('/www', function (req, res, next) {
  res.send(req.query);
  return next();
});
// *menggunakan query

server.listen(2000, function () {
  console.log('%s listening at %s', server.name, server.url);
});