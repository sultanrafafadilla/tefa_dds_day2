var restify = require('restify');
 
const server = restify.createServer({
  name: 'myapp',
  version: '1.0.0'
});
 
server.use(restify.plugins.acceptParser(server.acceptable));
server.use(restify.plugins.queryParser());
server.use(restify.plugins.bodyParser());
 
server.get('/TEFA/:alas/:tinggi', function(req,res,next){
    let luas = (req.params.alas/ 2) * req.params.tinggi ;
    res.send("luas = "+luas);
    return next();
});

server.get('/TEFA', function(req,res,next){
    let luas = (req.query/2) * req.query;
    res.send("luas = "+luas);
    return next();
});

 
server.listen(2000, function () {
  console.log('%s listening at %s', server.name, server.url);
});